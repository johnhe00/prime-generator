#include "stdio.h"
#include "stdlib.h"
#include "math.h"

struct List {
	unsigned int val;
	struct List * next;
};

typedef struct List list;

int main(int argc, char * argv[]) {
	unsigned long num;
	switch(argc) {
	case 2:
		num = atol(argv[1]);
		break;
	default:
		fputs("Usage: ./prime <long int>\n", stderr);
		return 1;
	}

	short size = sizeof(list);
	unsigned long totalSize = size;

	list * primes = malloc(size);
	list * last = primes;
	primes->val = 2;
	primes->next = NULL;

	for(unsigned long i = 3; i < num; i++) {
		list * p = primes;
		while(p) {
			if(p->val > sqrt(i)) {
				p = NULL;
				break;
			}
			if (i % p->val == 0) {
				break;
			}
			p = p->next;
		}
		if(!p) {
			list * nextPrime = malloc(size);
			totalSize += size;
			if(!nextPrime) {
				fputs("ERROR: OUT OF MEMORY\n", stderr);
				exit(1);
			}
			nextPrime->val = i;
			nextPrime->next = NULL;

			last->next = nextPrime;
			last = nextPrime;
			
			printf("%lu\n", i);
		}
	}
	
	while(primes) {
		list * temp = primes;
		primes = primes->next;
		free(temp);
	}

	fprintf(stderr, "Process complete: %lu bytes allocated\n", totalSize);

	return 0;
}
