This is my approach to finding prime numbers in a given range.

Other programs may increment the counter by 2 and check every number up to the square root of the counter.

But, my program utilizes the fact that a composite number can be factored into prime numbers. In other words, a number is prime if it does not divide any prime number smaller than its square root evenly.
